from second.pytorch.train import prune
from brevitas.core.quant import QuantType
from brevitas.core.scaling import ScalingImplType
import brevitas.nn as qnn
from torchplus.tools import change_default_args
import torch.nn as nn

from second.pytorch.models.quantization import QuantConfig

import time

# # Example:
# EXPERIMENTS.append({ 
#     "first_quant"      : QuantType.FP,
#     "first_bit_width"  : 32,
#     "middle_quant"     : QuantType.FP,
#     "middle_bit_width" : 32,
#     "last_quant"       : QuantType.FP,
#     "last_bit_width"   : 32,
#     "activation_quant"     : QuantType.FP,
#     "activation_bit_width" : 32,
#     "activation_type"      : qnn.QuantReLU,
#     "activation_max_val"   : 6,
#     "experiment_name"      : "test_one" 
# })

#python ./pytorch/train.py train --config_path=./configs/pointpillars/car/xyres_16.proto --model_dir=../model16_test

CONFIG_PATH      = "./configs/pointpillars/car/xyres_16.proto"
MODEL_DIR_FORMAT = "../model16_{name}"
EXPERIMENTS = []

# # Example:
EXPERIMENTS.append({ 
    "first_quant"      : QuantType.FP,
    "first_bit_width"  : 32,
    "middle_quant"     : QuantType.FP,
    "middle_bit_width" : 32,
    "last_quant"       : QuantType.FP,
    "last_bit_width"   : 32,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "test_one",
    "pruning_percent"      : 80
})


def main():
    elapsed_time = {}
    for i, experiment in enumerate(EXPERIMENTS):
        try:
            QuantConfig.FIRST_LAYER_QUANT_TYPE = experiment.get("first_quant")
            QuantConfig.FIRST_LAYER_BIT_WIDTH  = experiment.get("first_bit_width")
            
            QuantConfig.WEIGHT_QUANT_TYPE = experiment.get("middle_quant")
            QuantConfig.WEIGHT_BIT_WIDTH  = experiment.get("middle_bit_width")
            
            QuantConfig.LAST_LAYER_QUANT_TYPE = experiment.get("last_quant")
            QuantConfig.LAST_LAYER_BIT_WIDTH  = experiment.get("last_bit_width")
            
            tmp_qt = experiment.get("backbone_conv_quant_type")
            tmp_bw = experiment.get("backbone_conv_bit_width")
            if tmp_qt is None or tmp_bw is None:
                QuantConfig.BACKBONE_CONV_QUANT_TYPE = experiment.get("middle_quant")
                QuantConfig.BACKBONE_CONV_BIT_WIDTH  = experiment.get("middle_bit_width")
            else:
                QuantConfig.BACKBONE_CONV_QUANT_TYPE = tmp_qt
                QuantConfig.BACKBONE_CONV_BIT_WIDTH  = tmp_bw

            tmp_qt = experiment.get("backbone_deconv_quant_type")
            tmp_bw = experiment.get("backbone_deconv_bit_width")
            if tmp_qt is None or tmp_bw is None:
                QuantConfig.BACKBONE_DECONV_QUANT_TYPE = experiment.get("middle_quant")
                QuantConfig.BACKBONE_DECONV_BIT_WIDTH  = experiment.get("middle_bit_width")
            else:
                QuantConfig.BACKBONE_DECONV_QUANT_TYPE = tmp_qt
                QuantConfig.BACKBONE_DECONV_BIT_WIDTH  = tmp_bw

            QuantConfig.ACTIVATION_QUANT_TYPE = experiment.get("activation_quant")
            QuantConfig.ACTIVATION_BIT_WIDTH  = experiment.get("activation_bit_width")

            QuantConfig.ACTIVATION_FUNCTION = change_default_args( 
                max_val           = experiment.get("activation_max_val"),
                quant_type        = experiment.get("activation_quant"),
                bit_width         = experiment.get("activation_bit_width"),
                scaling_impl_type = ScalingImplType.CONST
            )(experiment.get("activation_type"))
            QuantConfig.FIRST_LAYER_ACTIVATION_FUNCTION = QuantConfig.ACTIVATION_FUNCTION

            model_dir = MODEL_DIR_FORMAT.format(name=experiment.get("experiment_name"))
            print("Pruning experiment {} (number {})".format(experiment.get("experiment_name"), i))
            time_start = time.time()
            pruning(
                config_path = CONFIG_PATH,
                model_dir   = model_dir,
                pruning_percent = experiment.get("pruning_percent")
            )
            time_end = time.time()
            elapsed_time[experiment.get("experiment_name")] = time_end - time_start
        except Exception as e:
            print("Exception while exp. num. {}: {}".format(i, e))
    
    print("\n")
    for k,v in elapsed_time.items():
        print("Experiment: {}\nElapsed time: {}\n".format(k, v))

if __name__ == "__main__":
    main()
