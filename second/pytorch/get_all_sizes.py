from second.pytorch.train import size_measurement
from brevitas.core.quant import QuantType
from brevitas.core.scaling import ScalingImplType
import brevitas.nn as qnn
from torchplus.tools import change_default_args
import torch.nn as nn

import json

from second.pytorch.models.quantization import QuantConfig

import time

# # Example:
# EXPERIMENTS.append({ 
#     "first_quant"      : QuantType.FP,
#     "first_bit_width"  : 32,
#     "middle_quant"     : QuantType.FP,
#     "middle_bit_width" : 32,
#     "last_quant"       : QuantType.FP,
#     "last_bit_width"   : 32,
#     "activation_quant"     : QuantType.FP,
#     "activation_bit_width" : 32,
#     "activation_type"      : qnn.QuantReLU,
#     "activation_max_val"   : 6,
#     "experiment_name"      : "test_one" 
# })

CONFIG_PATH      = "./configs/pointpillars/car/xyres_16.proto"
MODEL_DIR_FORMAT = "../model16_{name}"
EXPERIMENTS = []


EXPERIMENTS.append({ 
    "first_quant"      : QuantType.FP,
    "first_bit_width"  : 32,
    "middle_quant"     : QuantType.FP,
    "middle_bit_width" : 32,
    "last_quant"       : QuantType.FP,
    "last_bit_width"   : 32,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_01_00"
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.FP,
    "first_bit_width"  : 32,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 8,
    "last_quant"       : QuantType.FP,
    "last_bit_width"   : 32,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_01_01" 
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.FP,
    "first_bit_width"  : 32,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 2,
    "last_quant"       : QuantType.FP,
    "last_bit_width"   : 32,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_01_02" 
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.FP,
    "first_bit_width"  : 32,
    "middle_quant"     : QuantType.BINARY,
    "middle_bit_width" : 1,
    "last_quant"       : QuantType.FP,
    "last_bit_width"   : 32,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_01_03" 
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.FP,
    "first_bit_width"  : 32,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 32,
    "last_quant"       : QuantType.FP,
    "last_bit_width"   : 32,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_01_04" 
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.FP,
    "first_bit_width"  : 32,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 16,
    "last_quant"       : QuantType.FP,
    "last_bit_width"   : 32,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_01_05" 
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.FP,
    "first_bit_width"  : 32,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 4,
    "last_quant"       : QuantType.FP,
    "last_bit_width"   : 32,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_01_06" 
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.FP,
    "first_bit_width"  : 32,
    "middle_quant"     : QuantType.BINARY,
    "middle_bit_width" : 1,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 16,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_02_01" 
})


EXPERIMENTS.append({ 
    "first_quant"      : QuantType.FP,
    "first_bit_width"  : 32,
    "middle_quant"     : QuantType.BINARY,
    "middle_bit_width" : 1,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 8,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_02_02" 
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.FP,
    "first_bit_width"  : 32,
    "middle_quant"     : QuantType.BINARY,
    "middle_bit_width" : 1,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 2,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_02_03" 
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.FP,
    "first_bit_width"  : 32,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 2,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 16,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_02_04" 
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.FP,
    "first_bit_width"  : 32,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 2,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 8,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_02_05" 
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.FP,
    "first_bit_width"  : 32,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 2,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 4,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_02_06" 
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.FP,
    "first_bit_width"  : 32,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 2,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 2,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_02_07" 
})


EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 8,
    "middle_quant"     : QuantType.BINARY,
    "middle_bit_width" : 1,
    "last_quant"       : QuantType.FP,
    "last_bit_width"   : 32,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_03_02" 
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 16,
    "middle_quant"     : QuantType.BINARY,
    "middle_bit_width" : 1,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 8,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_03_04" 
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 8,
    "middle_quant"     : QuantType.BINARY,
    "middle_bit_width" : 1,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 8,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_03_05" 
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 16,
    "middle_quant"     : QuantType.BINARY,
    "middle_bit_width" : 1,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 16,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_03_06" 
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 16,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 2,
    "last_quant"       : QuantType.FP,
    "last_bit_width"   : 32,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_03_07" 
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 8,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 2,
    "last_quant"       : QuantType.FP,
    "last_bit_width"   : 32,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_03_08" 
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 16,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 2,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 16,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_03_09" 
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 4,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 2,
    "last_quant"       : QuantType.FP,
    "last_bit_width"   : 32,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_03_10" 
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 8,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 2,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 8,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_03_11" 
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 16,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 2,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 8,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_03_12" 
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 2,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 2,
    "last_quant"       : QuantType.FP,
    "last_bit_width"   : 32,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_03_13" 
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 16,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 2,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 2,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_03_14" 
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 8,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 2,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 2,
    "activation_quant"     : QuantType.FP,
    "activation_bit_width" : 32,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_03_15" 
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 16,
    "middle_quant"     : QuantType.BINARY,
    "middle_bit_width" : 1,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 16,
    "activation_quant"     : QuantType.INT,
    "activation_bit_width" : 16,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_04_03"
})


EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 16,
    "middle_quant"     : QuantType.BINARY,
    "middle_bit_width" : 1,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 16,
    "activation_quant"     : QuantType.INT,
    "activation_bit_width" : 8,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_04_04"
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 16,
    "middle_quant"     : QuantType.BINARY,
    "middle_bit_width" : 1,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 16,
    "activation_quant"     : QuantType.INT,
    "activation_bit_width" : 2,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_04_05"
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 16,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 2,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 16,
    "activation_quant"     : QuantType.INT,
    "activation_bit_width" : 8,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_04_06"
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 16,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 2,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 16,
    "activation_quant"     : QuantType.INT,
    "activation_bit_width" : 2,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_04_07"
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 16,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 2,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 16,
    "activation_quant"     : QuantType.INT,
    "activation_bit_width" : 16,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_04_08"
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 16,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 2,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 8,
    "activation_quant"     : QuantType.INT,
    "activation_bit_width" : 16,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_04_09"
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 16,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 2,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 8,
    "activation_quant"     : QuantType.INT,
    "activation_bit_width" : 8,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_04_10"
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 16,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 2,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 8,
    "activation_quant"     : QuantType.INT,
    "activation_bit_width" : 4,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_04_11"
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 16,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 2,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 8,
    "activation_quant"     : QuantType.INT,
    "activation_bit_width" : 2,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_04_12"
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 16,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 2,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 16,
    "activation_quant"     : QuantType.INT,
    "activation_bit_width" : 4,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_04_13"
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 8,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 2,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 8,
    "activation_quant"     : QuantType.INT,
    "activation_bit_width" : 16,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_04_14"
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 8,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 2,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 8,
    "activation_quant"     : QuantType.INT,
    "activation_bit_width" : 8,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_04_15"
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 8,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 2,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 8,
    "activation_quant"     : QuantType.INT,
    "activation_bit_width" : 4,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_04_16"
})

EXPERIMENTS.append({ 
    "first_quant"      : QuantType.INT,
    "first_bit_width"  : 8,
    "middle_quant"     : QuantType.INT,
    "middle_bit_width" : 2,
    "last_quant"       : QuantType.INT,
    "last_bit_width"   : 8,
    "activation_quant"     : QuantType.INT,
    "activation_bit_width" : 2,
    "activation_type"      : qnn.QuantReLU,
    "activation_max_val"   : 6,
    "experiment_name"      : "Eksperyment_04_17"
})

def main():
    network_sizes = {}
    for i, experiment in enumerate(EXPERIMENTS):
        try:
            QuantConfig.FIRST_LAYER_QUANT_TYPE = experiment.get("first_quant")
            QuantConfig.FIRST_LAYER_BIT_WIDTH  = experiment.get("first_bit_width")
            
            QuantConfig.WEIGHT_QUANT_TYPE = experiment.get("middle_quant")
            QuantConfig.WEIGHT_BIT_WIDTH  = experiment.get("middle_bit_width")
            
            QuantConfig.LAST_LAYER_QUANT_TYPE = experiment.get("last_quant")
            QuantConfig.LAST_LAYER_BIT_WIDTH  = experiment.get("last_bit_width")
            
            QuantConfig.ACTIVATION_QUANT_TYPE = experiment.get("activation_quant")
            QuantConfig.ACTIVATION_BIT_WIDTH  = experiment.get("activation_bit_width")

            QuantConfig.ACTIVATION_FUNCTION = change_default_args( 
                max_val           = experiment.get("activation_max_val"),
                quant_type        = experiment.get("activation_quant"),
                bit_width         = experiment.get("activation_bit_width"),
                scaling_impl_type = ScalingImplType.CONST
            )(experiment.get("activation_type"))
            QuantConfig.FIRST_LAYER_ACTIVATION_FUNCTION = QuantConfig.ACTIVATION_FUNCTION

            model_dir = MODEL_DIR_FORMAT.format(name=experiment.get("experiment_name"))
            print("Training experiment {} (number {})".format(experiment.get("experiment_name"), i))
            size_dict = size_measurement(
                config_path = CONFIG_PATH,
                model_dir   = model_dir
            )
            network_sizes[experiment.get("experiment_name")] = size_dict
        except Exception as e:
            print("Exception while exp. num. {}: {}".format(i, e))
    
    print("\n")
    content = json.dumps(network_sizes, indent=4, sort_keys=True)
    with open("network_sizes.json", "wt") as file:
        file.write(content)

if __name__ == "__main__":
    main()
