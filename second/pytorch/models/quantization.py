from brevitas.core.quant import QuantType
from brevitas.core.scaling import ScalingImplType
from brevitas.core.restrict_val import RestrictValueType
import brevitas.nn as qnn
from torchplus.tools import change_default_args
import torch.nn as nn

class MyQuantReLU(nn.Module):
    def __init__(self, max_val, quant_type, bit_width, scaling_impl_type=ScalingImplType.CONST, restrict_scaling_type=RestrictValueType.LOG_FP):
        super(MyQuantReLU, self).__init__()
        self._min_val_act = -1
        self._max_val_act = 1 - 2/(2**bit_width)
        self._min_val = 0
        self._max_val = max_val
        self._act = qnn.QuantHardTanh(scaling_impl_type=scaling_impl_type, restrict_scaling_type=restrict_scaling_type, min_val=self._min_val_act, max_val=self._max_val_act, quant_type=quant_type, bit_width=bit_width)
        self._scale = (self._max_val - self._min_val) / (self._max_val_act - self._min_val_act)

    def forward(self, x):
        x = self._act(x / self._scale - 1)
        x = self._scale * x
        x = x + self._scale
        return x

class QuantConfig():
    # Specify quantization of first layer
    FIRST_LAYER_QUANT_TYPE = QuantType.FP
    FIRST_LAYER_BIT_WIDTH  = 32
    FIRST_LAYER_ACTIVATION_QUANT_TYPE = QuantType.FP
    FIRST_LAYER_ACTIVATION_BIT_WIDTH  = 32
    FIRST_LAYER_ACTIVATION_FUNCTION   = change_default_args( max_val=6, quant_type=FIRST_LAYER_ACTIVATION_QUANT_TYPE, bit_width=FIRST_LAYER_ACTIVATION_BIT_WIDTH, scaling_impl_type=ScalingImplType.CONST)(qnn.QuantReLU)

    # Specify quantization of all the other layers
    WEIGHT_QUANT_TYPE      = QuantType.FP
    WEIGHT_BIT_WIDTH       = 32

    # Specify quantization of backbone inner conv blocks
    BACKBONE_CONV_QUANT_TYPE = QuantType.FP
    BACKBONE_CONV_BIT_WIDTH  = 32

    # Specify quantization of backbone inner deconv blocks
    BACKBONE_DECONV_QUANT_TYPE = QuantType.FP
    BACKBONE_DECONV_BIT_WIDTH  = 32

    # Specify quantization of last layer
    LAST_LAYER_QUANT_TYPE = QuantType.FP
    LAST_LAYER_BIT_WIDTH  = 32

    # Specify quantization of activation function
    ACTIVATION_QUANT_TYPE = QuantType.FP
    ACTIVATION_BIT_WIDTH  = 32
    ACTIVATION_FUNCTION   = change_default_args( max_val=6, quant_type=ACTIVATION_QUANT_TYPE, bit_width=ACTIVATION_BIT_WIDTH, scaling_impl_type=ScalingImplType.CONST)(qnn.QuantReLU)
