import torch
import torch.nn as nn
import brevitas.nn as qnn
from brevitas.core.quant import QuantType
from second.pytorch.models.quantization import QuantConfig

class QuantConvTranspose2d(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0, bias=True, weight_quant_type=QuantConfig.WEIGHT_QUANT_TYPE, weight_bit_width=QuantConfig.WEIGHT_BIT_WIDTH):
        super(QuantConvTranspose2d, self).__init__()
        self.stride = stride
        self.p = kernel_size - padding - 1

        self.conv = qnn.QuantConv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size, padding=self.p, bias=bias, weight_quant_type=weight_quant_type, weight_bit_width=weight_bit_width)
        #self.conv = nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size, padding=self.p, bias=bias)
    
    def _gen_bigger_img(self, x):
        shape = x.shape
        x_type = x.dtype
        x_device = x.device
        new_shape = list(shape)
        z = self.stride - 1 
        new_shape[2] = new_shape[2] + z * (new_shape[2] - 1)
        new_shape[3] = new_shape[3] + z * (new_shape[3] - 1)
        new_tensor = torch.zeros(new_shape, dtype=x_type, device=x_device)

        step  = z+1
        seq_i = torch.arange(0, shape[2]*step, step, dtype=torch.long, device=x_device)
        seq_j = torch.arange(0, shape[3]*step, step, dtype=torch.long, device=x_device)
        iv, jv = torch.meshgrid(seq_i, seq_j)
        new_tensor[:, :, iv, jv] = x
        
        # Delete not used variables
        del seq_i
        del seq_j
        del iv
        del jv

        return new_tensor

    def forward(self, x):
        bigger_img = self._gen_bigger_img(x)
        x = self.conv(bigger_img)
        del bigger_img # delete not used tensor
        return x